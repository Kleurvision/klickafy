<?php

function createguid(){
    $data = openssl_random_pseudo_bytes(16);

    $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
}

function arrayDisplay($array) {
	if(DEBUG == true || $environment == 'omari'){
		echo "<pre>";
		echo print_r($array);
		echo "</pre>";
	}
}

function upload_file($file, $uploadType = 'image'){
    $target_dir = UPLOADDIR;
    if($uploadType == 'image' OR $uploadType == 'audio'){

        $target_file = $target_dir . basename($file["name"]);
        $uploadOk = 1;
        $fileType = pathinfo($target_file,PATHINFO_EXTENSION);
        $randomName = createguid();
        $temp = explode(".", $file["name"]);
        $newfilename = $randomName . '.' . end($temp);
        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($file["tmp_name"]);
            if($check !== false) {
                return "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                return "File is not an image.";
                $uploadOk = 0;
            }
        }
        // Check if file already exists
        if (file_exists($target_file)) {
            return "Sorry, file already exists.";
            $uploadOk = 0;
        }
        // Check file size
        if ($file["size"] > 10485760) {
            return "Sorry, your file is too large.";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($uploadType == 'image' AND ($fileType != "jpg" && $fileType != "JPG" && $fileType != "png" && $fileType != "jpeg" && $fileType != "gif") ) {
            return "Sorry, we only allow JPG, JPEG, PNG & GIF files for image upload.";
            $uploadOk = 0;
        } else if($uploadType == 'audio' AND ($fileType != "mp3" && $fileType != "mp4" && $fileType != "m4a" && $fileType != "acc" && $fileType != "oga" && $fileType != "ogg" && $fileType != "wav") ) {
            return "Sorry, only MP3, MP4, OGA/OGG & WAV files are allowed.";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            return "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($file["tmp_name"], $target_dir . $newfilename)) {
                $return = array('status' => true, 'file_name' => $newfilename);
                return $return;
            } else {
                return "Sorry, there was an error uploading your file.";
            }
        }
    } else {
        return "Sorry, This file cannot be uploaded.";
    }
}
?>