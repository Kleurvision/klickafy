<?php
require('./vitals.php');
include( ROOT .'assets/php/header.php');

// this line loads the Twilio library 
require('/assets/Twilio/autoload.php');
use Twilio\Rest\Client;

/* Live Information */
$account_sid 		= 'ACdaa3abcdc623e9ab6becc7e94b309e36';
$auth_token 		= 'd733eebb14314944190caaad29c9a7e9';

/* Test Information 
$account_test_sid = 'ACcf6190b0e0479b60966173b06c282d6c';
$auth_test_token = '8aae99f626630c0aa74c1ebd9735fe55';*/

//Create Message Client
$client 			= new Client($account_sid, $auth_token);

$submitted_number 	= $_POST['phone_num'];
$to_number 			= explode(',', $submitted_number);
$from_test_number 	= '+15005550006';
$from_live_number 	= '+16476942375';
$message_body 		= $_POST['message'];
$submit 			= $_POST['submit_check'];
$msg_check 			= $_POST['message_check'];
$message_image		= $_POST['message_img'];

/* Tried to wrap image with link 
$sentmessage = $client->messages->create( 
    "+16472863113",
    array(
        'from' => "+16476942375",
        'body' => "Message with image and link",
        'mediaUrl' => "<a href='http://kleurvision.com'>https://kleurvision.com/wp-content/uploads/2015/02/kv_logo_600x200_w.png</a>",
    )
);*/
if($submit == 'true') {
	//echo "hey";
	foreach ($to_number as $key => $value) {
		$message_data 				= new stdClass();
		$to_num 					= '+1'.$value;
		$click_id					= createguid();
		$track_link 				= "http://local.klickafydemo.com/tracking.php?clickid=".$click_id;
		$message_data->tracklink 	= $track_link;
		$upload_img			= $db->escape($_FILES['upload_message_img']);

		/*File upload processing*/
		if($upload_img['name'] != '') {
			$file_info = upload_file($upload_img);
			if(is_array($file_info)) {
				$status = true;
			} else {
				$status = false;
			}
		} else {
			$status = true;
		}

		if($status == true) {
			if($file_info['file_name'] != "") {
	 			$file_name = $file_info['file_name'];
				echo $file_url  = URL."uploads/".$file_name;
			}

			if($message_image != "" && $message_image != " ") {

				$sentmessage = $client->messages->create( 
				    "$to_num",
				    array(
				        'from' => "$from_live_number",
				        'body' => "$message_body \n \n $track_link",
				        'mediaUrl' => "$message_image",
				    )
				);

			} else if ($file_name != ""){

				$sentmessage = $client->messages->create( 
				    "$to_num",
				    array(
				        'from' => "$from_live_number",
				        'body' => "$message_body \n \n $track_link",
				        'mediaUrl' => "$file_url",
				    )
				);

			} else {
				$sentmessage = $client->messages->create( 
				    "$to_num",
				    array(
				        'from' => "$from_live_number",
				        'body' => "$message_body \n $track_link",
				    )
				);
			}

			if($sentmessage) {
				$message_data->sid 		= $sentmessage->sid;
				$message_data->date 	= $sentmessage->dateSent->format('Y-m-d H:i:s');
				$message_data->tonumber = $sentmessage->to;
				$message_data->frnumber = $sentmessage->from;
				$message_data->status 	= $sentmessage->status;
				$message_data->msg 		= $sentmessage->body;
				$message_data->imglink	= $message_image;
				$message_data->imgname  = $file_name;

				echo "<pre>";
				print_r($sentmessage);
				echo "</pre>";
			}
			$guid						= createguid();
			$msg_data 					= json_encode($message_data);

			$insertMsgInfo = $db->query("INSERT INTO message_data
								
								(
									guid,
									cid,
									click_id,
									date_sent,
									message_info
									
										) VALUES (
									
									'$guid',
									'$sentmessage->sid',
									'$click_id',
									'$message_data->date',
									'$msg_data'
									
								)");

		}

	}
} else if($msg_check == 'true') {
	$message_id		= $_POST['message_id'];
	$tw_messageinfo = $client->messages("$message_id")->fetch();
	$internal_info	= $db->get_row("SELECT * FROM message_data WHERE cid = '$message_id'");
	
}

?>

<form role="form" action="" method="post" id="message_form" class="message-form" enctype="multipart/form-data">
	<div class="col-md-offset-3 col-md-6">
		<label class="phone_label">Phone Number</label>
		<input type="text" name="phone_num" class="phone_number">
		<br>
		<br>
		<label class="picture_label">Attach Image to Message (Enter in link to picture)</label>
		<input type="text" name="message_img" class="message image">
		<br>
		<br>
		<!-- image-preview-filename input [CUT FROM HERE]-->
        <div class="input-group image-preview">
        	<label class="picture_label">Or Upload Image Here</label>
            <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
            <span class="input-group-btn">
                <!-- image-preview-clear button -->
                <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                    <span class="glyphicon glyphicon-remove"></span> Clear
                </button>
                <!-- image-preview-input -->
                <div class="btn btn-default image-preview-input">
                    <span class="glyphicon glyphicon-folder-open"></span>
                    <span class="image-preview-input-title">Browse</span>
                    <input type="file" accept="image/png, image/jpeg, image/gif" name="upload_message_img"/> <!-- rename it -->
                </div>
            </span>
        </div><!-- /input-group image-preview [TO HERE]--> 
		<br>
		<br>
		<label class="message_label">Text Message
		<textarea name="message" rows="5" cols="120" class="message_body">
		</textarea>
		</label>
		<br>
		<br>
		<input type="hidden" name="submit_check" value="true">
		<input type="submit" name="submit_message" class="msg_submit_btn">
	</div>
</form>

<form role="form" action="" method="post" id="message_check" class="message-check-form">
	<div class="col-md-offset-3 col-md-6">
		<br>
		<br>
		<label class="phone_label">Message ID (SID)</label>
		<input type="text" name="message_id" class="message_id">
		<br>
		<br>
		<input type="hidden" name="message_check" value="true">
		<input type="submit" name="submit_check" class="msg_check_btn">

		<?php 
			if($tw_messageinfo != "") {
				echo "<h2>Twilio Message Info</h2>";
				echo "<pre>";
				print_r($tw_messageinfo);
				echo "</pre>";
			}

			if($internal_info != "") {
				echo "<h2>Internal Database Message Info</h2>";
				echo "<pre>";
				print_r($internal_info);
				echo "</pre>";
			}
			
		?>
	</div>
</form>

<form role="form" action="" method="post" id="display_message" class="display-messages-form">
	<div class="col-md-offset-3 col-md-6">
		<br>
		<br>
		<label class="phone_label">Display All Messages</label>
		<br>
		<br>
		<input type="hidden" name="all_messages" value="true">
		<input type="submit" name="display_messages" class="all_msg_btn">

		<?php 
			$all_msgs = $_POST['all_messages'];

			if($all_msgs == 'true') {
				$internal_messages	= $db->get_results("SELECT message_info FROM message_data");
				// Loop over the list of messages and echo a property for each one
				$counter = 1;
				foreach ($internal_messages as $message) {
					$message_info = json_decode($message->message_info);
					//print_r($message_info);

				    echo "<h4>Message ".$counter."</h4>";
				    echo "<pre>";
					echo "SID: ".$message_info->sid."</br>";
					echo "Message Send Date: ".$message_info->date."</br>";
					echo "To #: ".$message_info->tonumber."</br>";
					echo "Fr #: ".$message_info->frnumber."</br>";
					echo "Status of Request: ".$message_info->status."</br>";
					echo "Message Body: ".$message_info->msg."</br>";
					if($message_info->imglink) {
						echo "Link of Image Sent: ".$message_info->imglink."</br>";
					}
					echo "</pre>";
					$counter++;
				}
			}
		?>
	</div>
</form>



<?php
//https://kleurvision.com/wp-content/uploads/2015/02/kv_logo_600x200_w.png
include( ROOT .'assets/php/footer.php');
?>