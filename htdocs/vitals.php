<?php

	/*  EzSQL
	***********************************************************************/
	error_reporting(E_ALL ^ E_NOTICE);

	$host = $_SERVER['SERVER_NAME'];

	// Check environment for location
	$environment = $_SERVER['APPLICATION_ENV'];

	// Include Config

	if($environment == 'omari') {
		include("/../config.php");
	} else {
		include("/inet/www/vololoapp/config.php");
	}
	
	//Upload Directory Define
	define('UPLOADDIR', $_SERVER['DOCUMENT_ROOT'].'/uploads/');
	
	/* SALTED PASSWORD
	*********************************************************************/
	// These constants may be changed without breaking existing hashes.
	define("PBKDF2_HASH_ALGORITHM", "sha256");
	define("PBKDF2_ITERATIONS", 1000);
	define("PBKDF2_SALT_BYTES", 24);
	define("PBKDF2_HASH_BYTES", 24);

	define("HASH_SECTIONS", 4);
	define("HASH_ALGORITHM_INDEX", 0);
	define("HASH_ITERATION_INDEX", 1);
	define("HASH_SALT_INDEX", 2);
	define("HASH_PBKDF2_INDEX", 3);

	/* Load up the functions
	*********************************************************************/
	include_once "assets/php/functions.php";

	/* Class Builder
	***********************************************************************/
	require_once("assets/php/class-builder.php");

	/* Global Defines
	***********************************************************************/
	session_start();

?>